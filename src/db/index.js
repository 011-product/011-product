import db from './db';

const addData = async (type, data) => {
  try {
    return await db.collection(type).add(data).then(docRef => console.log(`${type} added to database id: ${docRef.id}`));
  } catch (e) {
    console.error(`Error adding ${type} to DB: ${e}`);
}

const getQueriedData = (type, variable, operand, condition) => {
  try {
    return db.collection(type).where(variable, operand, condition);
  } catch (e) {
    console.error(`Error getting ${type} query: (${variable}, ${operand}, ${condition}`);
  } finally {
    return []
  }
}

const getSponsorsByLevel = (level) => {
  switch (level) {
    case 'diamond':
      return getQueriedData('sponsors', 'type', '==', 'diamond');
    case 'platinum':
      return getQueriedData('sponsors', 'type', '==', 'platinum');
    case 'gold':
      return getQueriedData('sponsors', 'type', '==', 'gold');
    case 'silver':
      return getQueriedData('sponsors', 'type', '==', 'silver');
    default: 
      return getQueriedData('sponsors', 'type', '==', 'bronze');
  }
}

const getAllData = async (type) => {
  let data = [];
  try {
    await db.collection(type).onSnapshot(querySnapshot => {
      querySnapshot.forEach(doc => {
        let temp = doc.data();
        temp.id = doc.id;
        data.push(temp);
      })
    })
  } catch (e) {
    console.error(`Error pulling ${type}(s) from database`, e);
  } finally {
    return data;
  }
}

const addSponsor = (data) => {
  // Diamond Sponsors
  if(data.amount >= 10000.00){
    data.type = 'diamond';
    return addData('sponsors', data);
    }
  };
  // Platinum Sponsors
  if(data.amount >= 5000.00){
    data.type = 'platinum';
    return addData('sponsors', data);
  };
  // Gold Sponsors
  if(data.amount >= 1000.00){
    data.type = 'gold';
    return addData('sponsors', data);
  };
  // Silver Sponsors
  if(data.amount >= 500.00){
    return addData('sponsors', data);
  } else {
    // Bronze Sponsors
    data.type = 'bronze';
    return addData('sponsors', data);
  };
};
